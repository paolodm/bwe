import { Matches } from '../imports/api/rooms.js';

Router.route('/', function () {
    this.render('player');
});

Router.route('/create/', function () {
    this.render('moderator');
});

Router.route('/play/:_id', {
    name: 'play',
    path: '/play/:_id',
    template: 'game',
    waitOn: function() {
        return Meteor.subscribe('match', this.params._id);
    },
    action: function() {
        if (this.ready()) {
            this.render();
        } else {
            this.render('Loading...');
        }
    },
});