Rooms contain rooms:

room
    _id:         (id)
    code:       (String, 4 characters)
    started:    (date)
    ended:      (date)
    joinable:   (boolean)   // true if it's in lobby state
                            // false if it's in progress/finished
    notes:      (String)
    matches:    (array containing match)
    moderator:  (id)

match   // matches will have two rounds to demonstrate how thinking changes
        // however, it'll be between two different pairs for two rounds.
        // hence the round condition at the bottom?
    id:         (id)
    players: {
        a {
            id:     (id)
            color:  (0/1 for blue/white)
            score:  (integer)
        }
        b {
            id:     (id)
            color:  (0/1 for blue/white)
            score:  (integer)
        }
    }
    events: {   // contains events. technically, they should have
                // 2 minute max, so using the delta instead of the
                // date would be interesting, but probably trivial gain.
        event [
            timestamp:  (date)
            type:       (String: hover, select, choose)
            player:     (a/b)   // this should only be a until
                                // a choice is made, then it's only b, etc.
        ]
    }
    currentTurn:    (based off player)
    inProgress:     (boolean)
    started:        (date)
    finished:       (date)
    round:          (round 1 or round 2?)