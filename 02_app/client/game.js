import { Meteor } from 'meteor/meteor';
import { Matches } from '../imports/api/rooms.js';
import { Session } from 'meteor/session'
import { Template } from 'meteor/templating';

import './game.html';

Template.game.onCreated(function() {
    
});

Template.game.helpers({
    // Scoreboard
    scores: function() { 
        return Matches.findOne().scores;
    },

    // Game UI
    selected: function() {
        Session.set("selected", Matches.findOne().choice);
        $(".header").each(function() {
            if (Session.equals("selected", this.id)) {
                $(this).addClass("selected");
            } else {
                $(this).removeClass("selected");
            }
        });
    },
    hidden: function() {
        let select = Matches.findOne().choices.slice(-1)[0].substring(5, 6);
        if (select === 'A' || select === 'B') {
            if (select === 'A') {
                $("[id^=game_B]").hide();
            } else {
                $("[id^=game_A]").hide();
            }
        } else {
            $("[id^=game_]").show();
            $("#game_X_header").removeClass("selected");
            $("#game_Y_header").removeClass("selected");
        }
    },

    // Game Elements
    matchID: function() {
        matchID = Iron.controller().params._id;
        return matchID; 
    },
    roundCurrent: function() {
        return Matches.findOne().currentRound;
    },
    roundOdd: function() {
        return (Matches.findOne().currentRound % 2 === 1 ? true : false);
    },
    roundTimer: function() {
        // need to send the server time and let both clients
        // sync up on this time.
        return "02:00";
    },
    playerCurrent: function() {
        let match = Matches.findOne(),
            round = match.currentRound,
            phase = match.phase;

        if ((round % 2 === 1) && (phase === 0) || (round % 2 === 0) && (phase === 1)) {
            return "Blue";
        } else {
            return "White";
        }
    },
    selectionCurrent: function() {
        return (Matches.findOne().phase === 0 ? "A or B" : "X or Y");
    }
});

Template.game.events({
    'click .header'(event, template) {
        let phase = Matches.findOne().phase;
        if (phase === 0 && (event.target.id === "game_X_header" || event.target.id === "game_Y_header")) {
            return;
        } else if (phase === 1 && (event.target.id === "game_A_header" || event.target.id === "game_B_header")) {
            return;
        } else {
            choice = event.target.id;
            Session.set("selected", choice);
            Meteor.call('match.play.click', Iron.controller().params._id, Meteor.userId(), choice, new Date().getTime());
        }
    },
    'click #game-selection'(event, template) {
        Meteor.call('match.play.submit', Iron.controller().params._id, Meteor.userId(), choice, new Date().getTime());
    },
})