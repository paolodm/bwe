import { Meteor } from 'meteor/meteor';
import { Rooms } from '../imports/api/rooms.js';
import { Matches } from '../imports/api/rooms.js';
import { Session } from 'meteor/session';
import { Template } from 'meteor/templating';

import './home.html'

Template.player.onCreated(function onCreated() {
    Meteor.subscribe('rooms');
    Meteor.subscribe('matches');
});

Template.player.helpers({
    userID: function() {
        return Meteor.userId();
    },
    rooms: function() {
        return Rooms.find({}, { sort: { started: -1 }}).map(function(room){
            room.name = room.name;
            room.started = moment(room.started).fromNow();
            if (room.players && room.players.length) {
                room.players = room.players.length;
            } else {
                room.players = 0;
            }
            return room
        })
    },
    matches: function() {
        return Matches.find({ currentTurn: Meteor.userId() }, { sort: { started: -1 }}).map(function(match) {
            match.roomName = Rooms.findOne(match.roomID).name;
            match.matchID  = match._id;
            return match;
        });
    },
});

Template.room.helpers({
    joinable: function() {
        let room        = Rooms.findOne({ _id: this._id }),
            players     = room.players,
            roomStatus  = room.joinable,
            userID      = Meteor.userId(),
            joinable    = true;

        if (players && players.indexOf(userID) !== -1) {
            joinable = false;
        }
        if (roomStatus === false) {
            joinable = false;
        }

        return (joinable === true ? {} : {disabled: 'disabled'});
    },
});

Template.moderator.helpers({
    roomsMod: function() {
        return Rooms.find({}, { sort: { started: -1 }}).map(function(room){
            room.name = room.name;
            room.started = moment(room.started).fromNow();
            if (room.players && room.players.length) {
                room.players = room.players.length;
            } else {
                room.players = 0;
            }
            return room
        })
    }
});

Template.player.events({
    'click #moderate'() {
        Router.go('/create');
    },
    'click .room'() {
        Meteor.call('rooms.joinRoom', this._id, Meteor.userId());
    },
    'click .match'() {
        Router.go('play', { _id: this._id });
    }
});

Template.moderator.events({
    'click #back'() {
        Router.go('/');
    },
    'click #create'() {
        Meteor.call('rooms.createRoom', Meteor.user());
    },
    'click .room'() {
        Meteor.call('rooms.createMatches', this._id);
    },
});