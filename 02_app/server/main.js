import '../imports/api/rooms.js';
import '../imports/api/play.js';

if (Meteor.isServer) {
    Meteor.startup(function () {
        AccountsGuest.forced = true;
        AccountsGuest.enabled = true;
        AccountsGuest.anonymous = true;
    });
}