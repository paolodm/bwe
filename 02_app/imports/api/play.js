import { check } from 'meteor/check';
import { Matches } from './rooms.js';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

Meteor.methods({
    'match.play.click'(roomID, userID, choice, time) {
        Matches.update({ _id: roomID }, { $push: { events: createEvent(userID, "click", choice, time)}});
        Matches.update({ _id: roomID }, { $set: { choice: choice }});
    },
    'match.play.submit'(roomID, userID, choice, time) {
        checkIfUsersTurn(roomID, userID);
        Matches.update({ _id: roomID }, { $push: { events: createEvent(userID, "submit", choice, time)}});
        Matches.update({ _id: roomID }, { $push: { choices: choice }});
        if (Matches.findOne({ _id: roomID }).phase === 0) {
            Matches.update({ _id: roomID }, { $inc: { phase: 1 }});
        } else {
            calculateScore();
            Matches.update({ _id: roomID }, { $inc: { currentRound: 1 }});
            Matches.update({ _id: roomID }, { $set: { phase: 0 }});
        }
    },
});

function checkIfUsersTurn(roomID, userID) {
    let match   = Matches.findOne({ _id: roomID }),
        round   = match.currentRound,
        user    = userID;

    return true;
}

function calculateScore(roomID) {
    let match   = Matches.findOne({}),
        round   = match.currentRound,
        cell    = String(match.choices[(round * 2)-2].charAt(5).concat(match.choices[(round * 2)-1].charAt(5))),
        scores  = {};

    scores["round"] = round;
    // odd rounds
    if (round % 2 == 1) {
        if (cell === "AX") {
            scores["blue"] = 0;
            scores["white"] = 15;
        } else if (cell === "BX") {
            scores["blue"] = -10;
            scores["white"] = 10;
        } else if (cell === "BY") {
            scores["blue"] = 20;
            scores["white"] = 0;
        } else {
            scores["blue"] = 5;
            scores["white"] = 5;
        }
    // even rounds
    } else {
        if (cell === "AX") {
            scores["blue"] = 15;
            scores["white"] = 0;
        } else if (cell === "BX") {
            scores["blue"] = 10;
            scores["white"] = -10;
        } else if (cell === "BY") {
            scores["blue"] = 0;
            scores["white"] = 20;
        } else {
            scores["blue"] = 5;
            scores["white"] = 5;
        }
    }
    Matches.update({}, { $push: { scores: scores }});
}

function createEvent(userID, eventName, selection, timestamp) {
    var event = {
        player: userID,
        event: eventName,
        target: selection,
        time: timestamp,
    }
    return event;
}