import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Random } from 'meteor/random';

export const Rooms = new Mongo.Collection('rooms');
export const Matches = new Mongo.Collection('matches');

if (Meteor.isServer) {
    Meteor.publish('rooms', function () {
        return Rooms.find();
    });
    Meteor.publish('matches', function () {
        return Matches.find({});
    });
    Meteor.publish('match', function (matchID) {
        return Matches.find({ _id: matchID });
    });
}

Meteor.methods({
    'rooms.joinRoom'(roomID, user) {
        Rooms.update({ _id: roomID }, { $addToSet: { players: user }});
    },
    'rooms.createRoom'(user) {
        // Entire user object is passed to check that user is not a guest.
        
        Rooms.insert({ 
            joinable: true,
            name: makeRoomName(),
            owner: user._id,
            started: new Date(),
            phase: 0,
            players: [],
        });
    },
    'rooms.createMatches'(roomID) {
        var room = Rooms.findOne(roomID);
        room.players = shuffle(room.players);

        for (var i = 0; i < room.players.length; i += 2) {
            if (room.players[i + 1] !== undefined) {    
                Matches.insert(createMatch(room.players.slice(i, i + 2), roomID));
            }
        }
    }
});

function createMatch (users, roomID) {
    var match = {
        _id: Random.id(),
        choice: "",
        choices: [],
        currentRound: 1,
        currentTurn: [users[0], users[1]],
        events: [],
        inProgress: true,
        phase: 0,
        roomID: roomID,
        scores: [],
        started: new Date(),
    }
    return match;
}

function makeRoomName() {
    var text = '';
    var char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    for (var i = 0; i < 4; i++) {
        text += char.charAt(Math.floor(Math.random() * char.length));
    }
    return text;
}

function shuffle(array) {
    // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}