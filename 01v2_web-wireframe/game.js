// Track which header has been selected.
var ASelected, BSelected, XSelected, YSelected, stageTwo = false;
var stageOne = true;
var cellColorDefault = "#373A3C";
var cellColorHover = "#6A7073";
var cellColorSelected = "#477751";
var headerColorSelect = "#000050";

// Stage 1
if (stageOne) {
  $( document ).ready(function(){
    $('#game_A_header, #game_B_header').css("background-color", headerColorSelect);
  });
}

// If A is...
// hovered over.
$('#game_A_header').on('mouseenter',function(){
  if (stageOne && !ASelected) {
    $('#game_AX_cell, #game_AY_cell').css("background-color", cellColorHover);
  }
});

// hovered off of.
$('#game_A_header').on('mouseleave',function(){
  if (stageOne && !ASelected) {
    $('#game_AX_cell, #game_AY_cell').css("background-color", cellColorDefault); 
  }
});

// clicked.
$('#game_A_header').on('click',function(){
  if (stageOne) {
    ASelected = true;
    BSelected = false;
    $('#game_AX_cell, #game_AY_cell').css("background-color", cellColorSelected);
    $('#game_BX_cell, #game_BY_cell').css("background-color", cellColorDefault);
  }
});

// If B is...
$('#game_B_header').on('mouseenter',function(){
  if (stageOne && !BSelected) {
    $('#game_BX_cell, #game_BY_cell').css("background-color", cellColorHover);
  }
});

// hovered off of.
$('#game_B_header').on('mouseleave',function(){
  if (stageOne && !BSelected) {
    $('#game_BX_cell, #game_BY_cell').css("background-color", cellColorDefault); 
  }
});

// clicked. 
$('#game_B_header').on('click',function(){
  if (stageOne) {
    ASelected = false;
    BSelected = true;
    $('#game_AX_cell, #game_AY_cell').css("background-color", cellColorDefault);
    $('#game_BX_cell, #game_BY_cell').css("background-color", cellColorSelected);
  }
});

// These can only happen if Stage 2 is happening, so that's the precondition for all these checks.

// If X is...
// hovered over.
$('#game_X_header').on('mouseenter',function(){
  if (stageTwo && !XSelected) {
    $('#game_AX_cell, #game_BX_cell').css("background-color", cellColorHover);
  }
});

// hovered off of.
$('#game_X_header').on('mouseleave',function(){
  if (stageTwo && !XSelected) {
    $('#game_AX_cell, #game_BX_cell').css("background-color", cellColorDefault); 
  }
});

// clicked.
$('#game_X_header').on('click',function(){
  if (stageTwo && !XSelected) {
    XSelected = true;
    YSelected = false;
    $('#game_AX_cell, #game_BX_cell').css("background-color", cellColorSelected);
    $('#game_AY_cell, #game_BY_cell').css("background-color", cellColorDefault);
  }
});

// If Y is...
$('#game_Y_header').on('mouseenter',function(){
  if (stageTwo && !YSelected) {
    $('#game_AY_cell, #game_BY_cell').css("background-color", cellColorHover);
  }
});

// hovered off of.
$('#game_Y_header').on('mouseleave',function(){
  if (stageTwo && !YSelected) {
    $('#game_AY_cell, #game_BY_cell').css("background-color", cellColorDefault); 
  }
});

// clicked.
$('#game_Y_header').on('click',function(){
  if (stageTwo && !YSelected) {
    XSelected = false;
    YSelected = true;
    $('#game_AX_cell, #game_BX_cell').css("background-color", cellColorDefault);
    $('#game_AY_cell, #game_BY_cell').css("background-color", cellColorSelected);
  }
});

// Deal with the fact that the button's been clicked.
$('#game-selection').on('click',function(){
  if (stageOne && (ASelected || BSelected)) {
    $('#stage-one-turn').addClass('hidden');
    if (ASelected) {
      $('#game_B_header, #game_BX_cell, #game_BY_cell').addClass('hidden');
      $('#stage-two-Aturn').removeClass('hidden');
    } else if (BSelected) {
      $('#game_A_header, #game_AX_cell, #game_AY_cell').addClass('hidden');
      $('#stage-two-Bturn').removeClass('hidden');
    }
    $('#game_AX_cell, #game_AY_cell, #game_BX_cell, #game_BY_cell, #game_A_header, #game_B_header').css("background-color", cellColorDefault);
    $('#game_X_header, #game_Y_header').css("background-color", headerColorSelect)
    stageOne = false;
    stageTwo = true;
  } else if (stageTwo && (XSelected || YSelected)) {
    $('#game_X_header, #game_Y_header').css("background-color", cellColorDefault);
    $('#stage-two-Aturn').addClass('hidden');
    $('#stage-two-Bturn').addClass('hidden');
    if (XSelected) {
      $('#game_Y_header, #game_AY_cell, #game_BY_cell').addClass('hidden');
    } else if (YSelected) {
      $('#game_X_header, #game_AX_cell, #game_BX_cell').addClass('hidden');
    }
    if (ASelected && XSelected) {
      $('#stage-three-AXturn').removeClass('hidden');
    } else if (ASelected && YSelected) {
      $('#stage-three-AYturn').removeClass('hidden');
    } else if (BSelected && XSelected) {
      $('#stage-three-BXturn').removeClass('hidden');
    } else if (BSelected && YSelected) {
      $('#stage-three-BYturn').removeClass('hidden');
    }
  }
});